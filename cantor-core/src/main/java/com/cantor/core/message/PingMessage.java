package com.cantor.core.message;

public class PingMessage extends CantorMessage {
    @Override
    public int getMessageType() {
        return MessageTypes.PING.getValue();
    }
}
