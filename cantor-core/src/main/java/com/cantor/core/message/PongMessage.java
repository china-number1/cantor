package com.cantor.core.message;

public class PongMessage extends CantorMessage{
    @Override
    public int getMessageType() {
        return MessageTypes.PONG.getValue();
    }
}
