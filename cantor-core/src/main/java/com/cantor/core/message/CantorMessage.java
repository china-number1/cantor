package com.cantor.core.message;

import com.cantor.common.serializer.SerializationTypes;
import lombok.Data;

import java.io.Serializable;

/**
 * 抽象的消息类, 让子类继承一些共有的属性, 例如sequenceId, messageType等
 */
@Data
public abstract class CantorMessage implements Serializable {

    private byte[] magicNum = new byte[]{8,8,6,8};
    private long sequenceId;
    private byte version = 1;
    private int serializationType = SerializationTypes.PROTOBUF.getValue();
    private int messageType;
    private int len;

    public abstract int getMessageType();
}
