package com.cantor.core.message;

public enum MessageTypes {
    CANTOR_REQUEST(1),
    CANTOR_RESPONSE(2),
    PING(3),
    PONG(4);

    private int value;

    MessageTypes(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
