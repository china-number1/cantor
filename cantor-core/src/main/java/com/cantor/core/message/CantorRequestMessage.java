package com.cantor.core.message;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class CantorRequestMessage extends CantorMessage {

    // 要调用的服务名
    private String interfaceName;

    // 要盗用的服务的版本
    private String serviceVersion;

    // 要调用的接口的方法名
    private String methodName;

    // 要调用的接口的方法的返回值类型(当某方法返回值为void时反序列化会出错,因此弃用此字段)
    @Deprecated
    private Class<?> returnType;

    // 要调用的接口逇方法的参数数组
    private Class[] parameterTypes;

    // 调用的接口的方法的参数数组
    private Object[] parameterValue;

    @Override
    public int getMessageType() {
        return MessageTypes.CANTOR_REQUEST.getValue();
    }

}
