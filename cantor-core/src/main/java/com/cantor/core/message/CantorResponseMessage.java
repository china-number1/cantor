package com.cantor.core.message;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class CantorResponseMessage extends CantorMessage {

    // 服务调用完毕后的返回值
    private Object returnValue;

    // 服务调用时产生的异常
    private Exception exceptionValue;

    @Override
    public int getMessageType() {
        return MessageTypes.CANTOR_RESPONSE.getValue();
    }
}
