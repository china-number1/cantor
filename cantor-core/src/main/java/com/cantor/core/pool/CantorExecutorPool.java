package com.cantor.core.pool;

import java.util.concurrent.*;

/**
 * 程序全局线程池管理类
 */
public class CantorExecutorPool {

    private static final ExecutorService executor;

    static {
        int nProcessors = Runtime.getRuntime().availableProcessors();
            executor = new ThreadPoolExecutor(
                nProcessors * 2,
                nProcessors * 10,
                60,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(nProcessors),
                new ThreadPoolExecutor.CallerRunsPolicy()
        );
    }

    // 把池子返回出去
    public static ExecutorService pool(){
        return executor;
    }

    // 提交一个Runnable任务
    public static void execute(Runnable runnable){
        executor.execute(runnable);
    }

    // 提交一个Future任务
    public static <T> Future<T> submit(Callable<T> callable){
        return executor.submit(callable);
    }

}
