package com.cantor.core.config;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CantorAppConfig {

    private CantorAppConfigProperties properties;

    public CantorAppConfig(CantorAppConfigProperties properties){
        this.properties = properties;
    }

}
