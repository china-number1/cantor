package com.cantor.core.handler;

import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

public class StickAndHalfPackageDecoder extends LengthFieldBasedFrameDecoder {
    /**
     * 协议头设计如下:
     * magicNum 魔数 4字节
     * sequenceId 分布式请求ID 8字节
     * version 协议版本 1字节
     * serializationType 序列化方式 4字节
     * messageType 业务消息类型 4字节
     * skip 无意义对齐字节 1字节
     * len 一个完整包的长度字段长度 4字节
     *
     * 共: 26字节
     */
    /**
     *          +----+--------+--+----+----+--+---+
     *          | 4  |    8   |1 |  4 |  4 |1 | 4 |
     *          +----+--------+--+----+----+--+---+
     */

    public StickAndHalfPackageDecoder() {
        this(2048, 22, 4, 0, 0);
    }

    public StickAndHalfPackageDecoder(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength, int lengthAdjustment, int initialBytesToStrip) {
        super(maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip);
    }
}
