package com.cantor.core.center;

import com.cantor.core.center.impl.ZooKeeperRegistrationCenter;
import org.apache.curator.framework.CuratorFramework;

import java.util.List;

/**
 * 负责维护与远程注册中心的连接
 * T 是注册中心的客户端的类型, 如果是zookeeper就是CuratorFramework
 */
public interface RegistrationCenter<T> {

    // 默认实现
    RegistrationCenter<CuratorFramework> DEFAULT_CENTER = new ZooKeeperRegistrationCenter();

    // 启动注册中心客户端
    void run();

    // 获取远程注册中心客户端
    T getClient();

    // 创建一个临时节点
    @Deprecated
    boolean createNodeE(String path);

    // 创建一个带数据的临时节点
    @Deprecated
    boolean createNodeE(String path,String data);

    // 创建持久节点
    @Deprecated
    boolean createNode(String path);

    // 创建带数据持久节点
    @Deprecated
    boolean createNode(String path,String data);

    // 删除一个临时节点
    void deleteNode(String... path);

    // 获取数据
    String getData(String... path);

    // 设置数据
    boolean setData(String data,String... path);

    // 获取所有子节点
    List<String>  getChildren(String... path);

    // 关闭连接
    boolean close();

    // 创建多级临时节点(自动拼接"/")
    boolean createMultiNode(String... paths);

    // 创建带数据的多级临时节点(自动拼接"/")
    boolean createMultiNodeWithData(String data,String... paths);

    // 创建多级临时节点(自动拼接"/")
    boolean createMultiNodeE(String... paths);

    // 创建带数据的多级临时节点(自动拼接"/")
    boolean createMultiNodeEWithData(String data,String... paths);

    // 判断节点是否存在
    boolean exists(String... paths);

    // 创建分布式id
    long getSequenceId() throws Exception;

}
