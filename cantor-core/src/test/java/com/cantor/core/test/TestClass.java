package com.cantor.core.test;

import cn.hutool.core.lang.Console;
import com.cantor.common.util.CantorUtil;
import com.cantor.core.center.RegistrationCenter;
import com.cantor.core.center.impl.ZooKeeperRegistrationCenter;
import com.google.common.base.Joiner;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class TestClass {

    private RegistrationCenter center;

    @BeforeEach
    public void init() {
        center = new ZooKeeperRegistrationCenter().setNamespace("cantor");
        center.run();
    }

    @AfterEach
    public void destroy() {
        center.close();
    }

    // 测试ZooKeeper注册中心实现类
    @Test
    public void t1() throws InterruptedException {
        Class<RegistrationCenter> itfc = RegistrationCenter.class;
        // 创建持久节点
//        System.err.println(center.createMultiNode(itfc.getName()));
        // 创建临时节点
        System.err.println(center.createMultiNodeEWithData("data",itfc.getName(),"1","2","3"));
        // 查看刚刚的节点的数据
        System.err.println(center.getData(itfc.getName(),"1","2","3"));
        // 查看子节点信息
        center.getChildren(itfc.getName()).forEach(System.err::println);
        Thread.sleep(Integer.MAX_VALUE);
    }

    // 测试google的Joiner类
    @Test
    public void testJoiner(){
        List<String> list = Arrays.asList("a");
        System.out.println("/"+Joiner.on("/").join(list));
    }

    // 测试远程注册中心的exists方法
    @Test
    public void testCenterExists() {
        System.err.println(center.exists("com.cantor.example.service.HelloService1"));
    }

    // 测试分布式Id速度
    @Test
    public void testSequenceIdSpeed() throws Exception {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            Console.log(center.getSequenceId());
        }
        Console.log("耗时: {}",System.currentTimeMillis() - start);
        /**
         * 测试结果: 100个id 700毫秒, 1000个id 2秒
         */
    }

}
