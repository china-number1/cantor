package com.cantor.common.serializer;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 该类用于包装要被序列化的对象, 具体指值放入内部的一个变量,
 * 这样就可以避免有的序列化方式, 不能直接序列化List, Map之类的类型, 包装一层之后就可以了.
 */
@Getter
@Setter
public class ToBeSerializedData<T> implements Serializable {
    @Expose
    private T target;
    public ToBeSerializedData(T target){
        this.target = target;
    }
}
