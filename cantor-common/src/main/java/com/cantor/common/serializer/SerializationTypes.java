package com.cantor.common.serializer;

public enum SerializationTypes {
    JDK(1),
//    JSON(2), // 有BUG, 禁用
//    KRYO(3), // 有BUG, 禁用
    PROTOBUF(4);

    private int value;

    SerializationTypes(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}