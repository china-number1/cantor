package com.cantor.common.serializer;

import com.cantor.common.util.ProtostuffUtil;

public class ProtobufSerializer implements Serializer{
    @Override
    public <T> byte[] serialize(T object) {
        return ProtostuffUtil.serialize(object);
    }

    @Override
    public <T> T deserialize(byte[] bytes, Class<T> clz) {
        return ProtostuffUtil.deserialize(bytes,clz);
    }
}
