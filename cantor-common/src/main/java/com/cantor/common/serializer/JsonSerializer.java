package com.cantor.common.serializer;

import com.cantor.common.util.GsonUtil;

/**
 * Json序列化器
 */
@Deprecated
public class JsonSerializer implements Serializer {

    @Override
    public <T> byte[] serialize(T object) {
        return GsonUtil.serialize(object);
    }

    @Override
    public <T> T deserialize(byte[] bytes, Class<T> clz) {
        return GsonUtil.deserialize(bytes, clz);
    }

}
