package com.cantor.common.serializer;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public interface Serializer {

    Map<Integer,Serializer> allSerializers = new ConcurrentHashMap<Integer,Serializer>(){{
        put(SerializationTypes.JDK.getValue(),new JdkSerializer());
//        put(SerializationTypes.JSON.getValue(), new JsonSerializer()); // Json方式有BUG, 禁用
//        put(SerializationTypes.KRYO.getValue(), new KryoSerializer()); // Kryo方式有BUG, 禁用
        put(SerializationTypes.PROTOBUF.getValue(), new ProtobufSerializer());
    }};

    // 序列化
    <T> byte[] serialize(T object);

    // 反序列化
    <T> T deserialize(byte[] bytes,Class<T> clz);

}
