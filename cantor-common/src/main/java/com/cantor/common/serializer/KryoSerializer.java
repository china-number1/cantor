package com.cantor.common.serializer;

import com.cantor.common.util.KryoUtil;

@Deprecated
public class KryoSerializer implements Serializer {

    @Override
    public <T> byte[] serialize(T object) {
        return KryoUtil.serializeObject(object);
    }

    @Override
    public <T> T deserialize(byte[] bytes, Class<T> clz) {
        return KryoUtil.unSerializeObject(bytes,clz);
    }
}
