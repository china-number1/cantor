package com.cantor.common.serializer;

import com.cantor.common.util.JdkUtil;

public class JdkSerializer implements Serializer {

    @Override
    public <T> byte[] serialize(T object) {
        return JdkUtil.serialize(object);
    }

    @Override
    public <T> T deserialize(byte[] bytes, Class<T> clz) {
        return JdkUtil.deserialize(bytes,clz);
    }
}
