package com.cantor.common.exception;

public class FailToConnectProviderException extends RuntimeException{
    public FailToConnectProviderException() {
        super("与Provider建立Netty连接超时");
    }
    public FailToConnectProviderException(String msg) {
        super(msg);
    }

}
