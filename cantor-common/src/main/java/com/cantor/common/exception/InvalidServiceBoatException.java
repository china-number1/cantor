package com.cantor.common.exception;

public class InvalidServiceBoatException extends RuntimeException {
    public InvalidServiceBoatException(){
        super("构建ServiceBoat时没有传入interface接口或实现类.");
    }

    public InvalidServiceBoatException(String msg){
        super(msg);
    }
}
