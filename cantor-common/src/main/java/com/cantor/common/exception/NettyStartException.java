package com.cantor.common.exception;

public class NettyStartException extends RuntimeException{
    public NettyStartException(){
        super("Netty启动失败.");
    }

    public NettyStartException(String msg){
        super(msg);
    }
}
