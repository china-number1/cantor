package com.cantor.common.exception;

public class NoSuchProviderOfServiceAvailableException  extends RuntimeException {
    public NoSuchProviderOfServiceAvailableException(String serviceName) {
        super("在注册中心中没有找到服务"+serviceName+"的Provider, 请检查其Provider是否已启动.");
    }
}
