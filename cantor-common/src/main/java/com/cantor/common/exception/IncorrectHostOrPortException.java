package com.cantor.common.exception;

public class IncorrectHostOrPortException  extends RuntimeException{
    public IncorrectHostOrPortException(){
        super("ConnectionKeeper.addConnection(host,port)方法无法对错误的地址建立连接");
    }
    public IncorrectHostOrPortException(String message) {
        super(message);
    }
}
