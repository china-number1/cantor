package com.cantor.common.exception;

public class NoSuchServiceImplException extends Exception{
    public NoSuchServiceImplException(){
        super("从注册者的Map中没有找到指定实现类");
    }

    public NoSuchServiceImplException(String msg){
        super(msg);
    }
}
