package com.cantor.common.exception;

public class RepetitiveRegisterException extends RuntimeException {
    public RepetitiveRegisterException(){
        super("同一个服务重复注册, 请取消注册或者修改重复服务的版本字符串");
    }

    public RepetitiveRegisterException(String msg){
        super(msg);
    }
}
