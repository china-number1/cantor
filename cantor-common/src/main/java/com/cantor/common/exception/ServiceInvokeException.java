package com.cantor.common.exception;

/**
 * 服务执行异常, 用于简化异常消息, 减小传输包大小
 */
public class ServiceInvokeException extends RuntimeException{

    public ServiceInvokeException(String message) {
        super(message);
    }
}
