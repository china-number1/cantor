package com.cantor.common.exception;

public class ComponentLackException extends RuntimeException{
    public ComponentLackException() {
        super("启动检查时发现缺少必要的组件,请确保builder()设置了必要值");
    }
    public ComponentLackException(String msg) {
        super(msg);
    }
}
