package com.cantor.cantorspringbootstarter.processor;

import com.cantor.provider.regsitry.ServiceRegistrant;
import com.cantor.provider.start.ProviderBootstrap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

/**
 * Spring启动监听器
 * 负责在Spring启动时, 启动Provider端
 */
public class ProviderAppRunner implements ApplicationRunner {

    @Autowired
    ServiceRegistrant serviceRegistrant;

    @Autowired
    ProviderBootstrap providerBootstrap;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // 没有任何服务提供者, 则不启动Provider, 节省资源
        if (serviceRegistrant.allServiceBoats().size() < 1) {
            return;
        }
        providerBootstrap.start();
    }

}
