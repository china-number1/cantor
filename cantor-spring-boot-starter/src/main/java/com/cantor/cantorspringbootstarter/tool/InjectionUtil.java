package com.cantor.cantorspringbootstarter.tool;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;

public class InjectionUtil {
    public static boolean isDefaultValue(Object object) {
        Class className = object.getClass();
        String strClassName = "" + className;
        if (className.equals(java.lang.Integer.class)) {
            return (int) object == 0;
        } else if (className.equals(java.lang.Byte.class)) {
            return (byte) object == 0;
        } else if (className.equals(java.lang.Long.class)) {
            return (long) object == 0L;
        } else if (className.equals(java.lang.Double.class)) {
            return (double) object == 0.0d;
        } else if (className.equals(java.lang.Float.class)) {
            return (float) object == 0.0f;
        } else if (className.equals(java.lang.Character.class)) {
            return (char) object == '\u0000';
        } else if (className.equals(java.lang.Short.class)) {
            return (short) object == 0;
        } else if (className.equals(java.lang.Boolean.class)) {
            return (boolean) object == false;
        } else if (className.equals(java.lang.String.class)) {
            return ObjectUtil.isEmpty(object) || StrUtil.isBlank((String) object);
        }
        return false;
    }
}
