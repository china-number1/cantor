package com.cantor.cantorspringbootstarter.tool;

import com.cantor.consumer.pojo.ServiceRef;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class FeignCache {
    public static final Map<String, ServiceRef> serviceRefCache = new ConcurrentHashMap();
}
