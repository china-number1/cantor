package com.cantor.cantorspringbootstarter.anno;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CantorService {
    /**
     * 版本
     * @return
     */
    String version() default "";

    /**
     * 高可用:超时时间
     */
    int timeout() default 5000;

    /**
     * 负载均衡: 权重
     */
    int weight() default 1;

    /**
     * 服务降级:强制执行策略:例如force:return null, fail:return null
     */
    String mock() default "";

}
