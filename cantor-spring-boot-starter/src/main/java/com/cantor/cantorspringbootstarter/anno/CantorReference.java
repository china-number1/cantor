package com.cantor.cantorspringbootstarter.anno;

import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CantorReference {

    /**
     * 版本
     * @return
     */
    String version() default "";

    /**
     * 负载均衡策略: random
     * @return
     */
    String loadBalance() default "";

    /**
     * 服务执行多久算超时?(毫秒)
     * @return
     */
    int timeout() default 5000;

    /**
     * 集群容错:重试次数
     * @return
     */
    int retries() default 0;

    /**
     * 服务降级:强制执行策略,例如force:return null, fail:return null
     * @return
     */
    String mock() default "";

}
