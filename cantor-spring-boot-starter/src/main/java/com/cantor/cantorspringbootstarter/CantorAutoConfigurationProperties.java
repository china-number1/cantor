package com.cantor.cantorspringbootstarter;

import cn.hutool.core.util.IdUtil;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "cantor")
public class CantorAutoConfigurationProperties {
    /**
     * App相关配置
     */
    // 应用名字
    private String appName = "Provider-" + IdUtil.fastSimpleUUID();

    // 服务端启动绑定的网关
    private String host = "127.0.0.1";

    // 服务端启动绑定的端口
    private Integer port = 8868;

    /**
     * Provider相关配置
     */
    private CantorAutoConfigurationProperties.ProviderConfig provider = new CantorAutoConfigurationProperties.ProviderConfig();

    @Data
    @Accessors(chain = true)
    public class ProviderConfig {

        // 灰度发布:服务版本
        private String version = "";

        // 高可用:超时时间
        private int timeout = 5000;

        // 负载均衡:权重
        private int weight = 1;

        // 服务降级:强制执行策略,例如force:return null, fail:return null
        private String mock = "";

    }

    /**
     * Consumer相关配置
     */
    private CantorAutoConfigurationProperties.ConsumerConfig consumer = new CantorAutoConfigurationProperties.ConsumerConfig();

    @Data
    @Accessors(chain = true)
    public class ConsumerConfig {

        // 灰度发布: 服务版本
        private String version = "";

        // 负载策略, 例如: "random","roundRobin"
        private String loadBalance = "random";

        // 高可用: 超时时间
        private int timeout = 10000;

        // 集群容错: 重试次数
        private int retries = 0;

        // 服务降级:强制执行策略,例如force:return null, fail:return null
        private String mock = "";

    }

    CantorAutoConfigurationProperties.CenterConfig center = new CenterConfig();
    @Data
    @Accessors(chain = true)
    public class CenterConfig {
        // 远程注册中心地址, 例如"127.0.0.1:2181"
        private String address = "127.0.0.1:2181";

        // 远程注册中心节点空间, 没有"/"
        private String namespace = "cantor";

        // 远程注册中心通信编码格式
        private String encoding = "UTF-8";

        // 分布式id节点名, 注意这里有"/",且不用重复写namespace
        private String sequenceIdZNode = "/seq";
    }

}
