package com.cantor.consumer.test;

import cn.hutool.core.lang.Console;
import cn.hutool.core.lang.WeightRandom;
import cn.hutool.core.util.RandomUtil;
import com.cantor.consumer.loadbalance.LoadBalance;
import com.cantor.consumer.loadbalance.strategy.RandomLoadBalanceStrategy;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 测试类
 */
public class TestClass {
    // 测试随机负载
    @Test
    public void tsetLoadBalance() {
        LoadBalance lb = new RandomLoadBalanceStrategy();
        Set<WeightRandom.WeightObj<String>> weightObjs = new HashSet<>(5);
        weightObjs.add(new WeightRandom.WeightObj<>("A", 1));
        weightObjs.add(new WeightRandom.WeightObj<>("B", 5));
        weightObjs.add(new WeightRandom.WeightObj<>("C", 10));
        weightObjs.add(new WeightRandom.WeightObj<>("D", 20));
        weightObjs.add(new WeightRandom.WeightObj<>("E", 50));

        // 取10000次
        int a = 0, b = 0, c = 0, d = 0, e = 0;
        for (int i = 0; i < 10000; i++) {
            String target = lb.select(weightObjs);
            switch (target) {
                case "A":
                    a++;
                    break;
                case "B":
                    b++;
                    break;
                case "C":
                    c++;
                    break;
                case "D":
                    d++;
                    break;
                case "E":
                    e++;
                    break;
                default:
                    break;
            }
        }
        // 看看每个对象被选择的次数
        System.out.println("A: " + a);
        System.out.println("B: " + b);
        System.out.println("C: " + c);
        System.out.println("D: " + d);
        System.out.println("E: " + e);

    }

    @Test
    public void send1000Times() {
        WeightRandom.WeightObj<String>[] weightObjs = new WeightRandom.WeightObj[]{
                new WeightRandom.WeightObj("8001", 5d),
                new WeightRandom.WeightObj("8002", 4d),
                new WeightRandom.WeightObj("8003", 3d),
                new WeightRandom.WeightObj("8004", 2d),
                new WeightRandom.WeightObj("8005", 1d)
        };
        Map<String, Integer> times = new ConcurrentHashMap<>();
        for (int i = 0; i < 1000; i++) {
            WeightRandom<String> selected = RandomUtil.weightRandom(weightObjs);
            String key = selected.next();
            if (!times.containsKey(key)) {
                times.put(key, 0);
            }
            times.put(key, times.get(key) + 1);
        }
        times.forEach((key, count) -> {
            Console.log("{} : {}", key, count);
        });
    }


}
