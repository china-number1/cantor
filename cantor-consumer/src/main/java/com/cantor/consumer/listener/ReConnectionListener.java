package com.cantor.consumer.listener;

import com.cantor.consumer.start.ConsumerNettyKeeper;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import lombok.extern.slf4j.Slf4j;

/**
 * CloseFuture的监听器, 负责断线重连
 */
@Slf4j
@Deprecated
public class ReConnectionListener implements ChannelFutureListener {

    private String host;

    private Integer port;

    public ReConnectionListener(String host, Integer port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public void operationComplete(ChannelFuture cf) throws Exception {
        // 如果channel非活跃, 就优雅关闭, 然后重新提交host,port给ConsumerNettyKeeper
        if(!cf.channel().isActive()){
            log.info("开始重连至{}:{}...",host,port);
            ConsumerNettyKeeper.addConnection(host,port);
        }
    }
}
