package com.cantor.consumer.discovery.impl;

import cn.hutool.core.net.url.UrlBuilder;
import com.cantor.common.exception.FailToConnectProviderException;
import com.cantor.common.util.CantorUtil;
import com.cantor.consumer.discovery.AbstractServiceDiscoverer;
import com.cantor.consumer.start.ConsumerNettyKeeper;
import com.cantor.core.center.RegistrationCenter;
import com.cantor.core.pool.CantorExecutorPool;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 默认服务发现者实现(功能在抽象类中已经这里, 这里在未来可以扩展一些其他功能, 比如监听节点变化)
 */
@Slf4j
@Deprecated
public class DefaultServiceDiscovererImpl extends AbstractServiceDiscoverer {


}
