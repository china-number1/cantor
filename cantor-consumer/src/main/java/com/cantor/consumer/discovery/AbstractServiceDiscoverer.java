package com.cantor.consumer.discovery;

import cn.hutool.core.net.url.UrlBuilder;
import com.cantor.common.exception.FailToConnectProviderException;
import com.cantor.common.util.CantorUtil;
import com.cantor.consumer.start.ConsumerNettyKeeper;
import com.cantor.core.center.RegistrationCenter;
import com.cantor.core.pool.CantorExecutorPool;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 抽象服务发现者
 */
@Slf4j
public abstract class AbstractServiceDiscoverer implements ServiceDiscoverer {

    // 使用Map<String,String> 对已经发现了的provider节点缓存
    protected Map<String,List<String>> cache = new ConcurrentHashMap<>();

    @Override
    @SneakyThrows
    public List<String> discover(RegistrationCenter center, String serviceNameWithVersion) {
        // 如果缓存中有, 直接返回
        if(cache.containsKey(serviceNameWithVersion)){
            log.debug("ServiceDiscoverer寻找节点触发缓存,{}已经存在, 当前map大小{}",serviceNameWithVersion, cache.size());
            return cache.get(serviceNameWithVersion);
        }
        // 从center中得到这个服务的所有provider注册的节点信息, 然后缓存到自己的map中, 并监听断开事件.
        List<String> nodes = center.getChildren(serviceNameWithVersion);
        // 准备用于缓存的List
        List<String> cacheList = new LinkedList<>();
        // 遍历这些节点, 通知ConnectionsKeeper建立连接, 直到建立成功后, 这个方法再return.
        CountDownLatch latch = new CountDownLatch(nodes.size());
        nodes.forEach(node -> {
            CompletableFuture.runAsync(()->{
                UrlBuilder urlBuilder = CantorUtil.getUrlBuilder(node);
                if (ConsumerNettyKeeper.addConnection(urlBuilder.getHost(), urlBuilder.getPort())) {
                    latch.countDown();
                    cacheList.add(node);
                    // 监听节点消失事件并通知Keeper断开连接.(暂时不写,让netty自动心跳超时断开连接)
                    // ...
                }
            },CantorExecutorPool.pool());
        });
        // 如果有部分Provider连接没成功,抛异常
        latch.await(5, TimeUnit.SECONDS);
        if (latch.getCount() != 0) {
            throw new FailToConnectProviderException();
        }
        cache.put(serviceNameWithVersion,cacheList); // 缓存
        return cacheList;
    }

}
