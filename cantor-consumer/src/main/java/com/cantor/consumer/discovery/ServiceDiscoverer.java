package com.cantor.consumer.discovery;

import com.cantor.consumer.discovery.impl.DefaultServiceDiscovererImpl;
import com.cantor.consumer.discovery.impl.NoCacheServiceDiscovererImpl;
import com.cantor.core.center.RegistrationCenter;

import java.util.List;

public interface ServiceDiscoverer {

    // 默认实现
    ServiceDiscoverer DEFAULT_DISCOVERER = new NoCacheServiceDiscovererImpl();

    // 传入服务名字符串,得到List<节点字符串>
    List<String> discover(RegistrationCenter center,String serviceNameWithVersion);

}
