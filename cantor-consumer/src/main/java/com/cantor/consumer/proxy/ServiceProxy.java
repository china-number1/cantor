package com.cantor.consumer.proxy;

public interface ServiceProxy {

    // 得到动态代理实现类
    <T> T ref();

}
