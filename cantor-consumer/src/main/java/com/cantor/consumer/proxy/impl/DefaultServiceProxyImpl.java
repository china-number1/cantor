package com.cantor.consumer.proxy.impl;

import com.cantor.consumer.pojo.ServiceRef;
import com.cantor.consumer.proxy.AbstractServiceProxy;

/**
 * 服务代理者默认实现
 */
public class DefaultServiceProxyImpl extends AbstractServiceProxy {

    public DefaultServiceProxyImpl(ServiceRef serviceRef) {
        super(serviceRef);
        serviceRef.refreshNodeList(); // 每次调用之前从zookeeper获得最新节点
    }

}
