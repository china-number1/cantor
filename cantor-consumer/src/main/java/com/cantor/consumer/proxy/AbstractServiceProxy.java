package com.cantor.consumer.proxy;

import com.cantor.consumer.pojo.ServiceRef;
import com.cantor.consumer.proxy.impl.DefaultInvocationHandler;

import java.lang.reflect.Proxy;

/**
 * 抽象动态代理者
 */
public abstract class AbstractServiceProxy<T> implements ServiceProxy {

    protected ServiceRef serviceRef;

    public AbstractServiceProxy(ServiceRef<T> serviceRef) {
        this.serviceRef = serviceRef;
    }

    @Override
    public <T> T ref() {
        Class<T> service = serviceRef.getService();
        // // 返回代理对象
        // return (T) Proxy.newProxyInstance(service.getClassLoader(), new Class[]{service}, new InvocationHandler() {
        //     @Override
        //     public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //         // 当某一方法调用, 交给Invoker执行这个方法
        //         return new DefaultConsumerServiceInvoker(method,args,serviceRef).invoke();
        //     }
        // });
        return (T) Proxy.newProxyInstance(service.getClassLoader(), new Class[]{service},new DefaultInvocationHandler(serviceRef));
    }


}
