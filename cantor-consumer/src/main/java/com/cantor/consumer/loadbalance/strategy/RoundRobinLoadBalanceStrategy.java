package com.cantor.consumer.loadbalance.strategy;

import cn.hutool.core.lang.WeightRandom;
import com.cantor.consumer.loadbalance.LoadBalance;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 平衡加权轮询负载策略
 */
@Deprecated
public class RoundRobinLoadBalanceStrategy implements LoadBalance {

    private static AtomicLong atomicIndex = new AtomicLong(0);

    @Override
    public <T> T select(Collection<WeightRandom.WeightObj<T>> weightObjs) {


        return null;
    }

    @Override
    public <T> Collection<T> select(Collection<WeightRandom.WeightObj<T>> weightObjs, int nSelected) {
        return null;
    }

}
