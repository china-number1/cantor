package com.cantor.consumer.loadbalance;

import cn.hutool.core.lang.WeightRandom;
import com.cantor.consumer.loadbalance.strategy.RandomLoadBalanceStrategy;

import java.util.Collection;

/**
 * 负载均衡策略接口
 */
public interface LoadBalance {
    <T> T select(Collection<WeightRandom.WeightObj<T>> weightObjs);
    <T> Collection<T> select(Collection<WeightRandom.WeightObj<T>> weightObjs, int nSelected);


    // 根据字符串返回一个具体的负载策略
    static LoadBalance getStrategy(String loadBalance){
        switch (loadBalance) {
            case "random":
                return new RandomLoadBalanceStrategy();
            default:
                return new RandomLoadBalanceStrategy();
        }
    }
}
