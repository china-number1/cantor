package com.cantor.consumer.invoker;


/**
 * Consumer端专用服务方法执行器接口
 * 并协调配合集群容错, 超时时间配置信息
 */
@Deprecated
public interface ConsumerServiceInvoker {
    Object invoke();
}
