package com.cantor.consumer.future;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 维护一个Map<String,CompletableFuture>, 负责接收结果.
 */
@Slf4j
public class FuturesKeeper {

    // 分布式id可以保证唯一, 故为了性能不使用CHM
    private static final Map<Long, CompletableFuture> futures = new HashMap<>();

    // 得到一个CompletableFuture
    // public static CompletableFuture get(Long sequenceId){
    //     log.trace("[分布式ID查看] {}",sequenceId);
    //     return futures.get(sequenceId);
    // }

    // 记录一个CompletableFuture
    public static CompletableFuture record(Long sequenceId, CompletableFuture future) {
        log.trace("[分布式ID记录开始] {}", sequenceId);
        futures.put(sequenceId, future); // 不能直接返回put方法的返回值, 会出现null, 原因不明
        return future;
    }

    // 检出一个CompletableFuture
    public static CompletableFuture checkout(Long sequenceId) {
        log.trace("[分布式ID检出完成] {}", sequenceId);
        return futures.remove(sequenceId);
    }

}
