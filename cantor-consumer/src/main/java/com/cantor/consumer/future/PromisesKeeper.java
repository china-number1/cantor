package com.cantor.consumer.future;

import io.netty.util.concurrent.Promise;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 维护一个Map<Long,Promise>, 负责结果的响应
 */
@Slf4j
@Deprecated
public class PromisesKeeper {

    private static final Map<Long, Promise> promises;

    static {
        promises = new ConcurrentHashMap<>();
    }

    // 得到一个Promise
    public static Promise get(Long sequenceId){
        log.debug("序列号任务{}被拿",sequenceId);
        return promises.get(sequenceId);
    }

    // 记录一个Promise
    public static Promise record(Long sequenceId, Promise promise){
        log.debug("序列号任务{}记录",sequenceId);
        return promises.put(sequenceId,promise);
    }

    // 检出一个Promise
    public static Promise checkout(Long sequenceId){
        log.debug("序列号任务{}检出",sequenceId);
        return promises.remove(sequenceId);
    }

}