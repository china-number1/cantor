# Cantor: 基于Netty+Protobuf的高性能RPC框架

### 介绍
<img src="https://thq-oss.oss-cn-beijing.aliyuncs.com/images/cantor_logo.jpg" alt="Cantor Logo" width="50" height="50" align="bottom" />

Cantor /ˈkæntɔː(r)/ 中文含义: 合唱指挥家.  
顾名思义, 在现如今微服务架构盛行的今天, Cantor在分布式系统中充当服务与消费者之间的指挥者, 让分布式架构的系统之间通信的音符协调运作. **Cantor 是一个基于Netty通信, Protobuf序列化的高性能RPC框架**,
经过半年的开发, 基本上实现了一个合格的RPC框架应有的功能, 例如: 数据传输, 分布式部署, SPI, 集群容错, 负载均衡, 灰度发布等.

### 功能特性:
- 基于RPC的数据传输
- 智能负载均衡
- Provider集群部署
- SPI高扩展性
- 集群容错
- 服务降级
- 灰度发布(同服务多版本发布)
- ...

### 所用技术:
- JDK动态代理
- 反射
- Netty
- NIO
- Google Protobuf
- CompletableFuture
- ZooKeeper
- Java SPI
- ...

### 快速开始
1. 导入starter依赖
```xml
<dependency>
    <groupId>com.cantor</groupId>
    <artifactId>cantor-spring-boot-starter</artifactId>
    <version>1.0</version>
</dependency>
```
> 由于Maven上传步骤太繁琐了, 请暂时先在当前页面下载源码后自行`package`

2. 配置yml

```yaml
cantor:
  app-name: HelloService # 服务提供者的名称
  port: 8868  # 服务提供者Netty绑定的端口
  center:
    address: 127.0.0.1:2181 # Zookeeper地址
```

3. 准备一个接口

```java
public interface HelloService {
    String sayHello(String name);
}
```

4. 和有`@CantorService`注解的实现类

```java
@CantorService
@Service
public class HelloServiceImpl implements HelloService {
    @Override
    public String sayHello(String word) {
        return "你好啊, " + word;
    }
}
```

5. 客户端只需要使用`@CantorReference`注解即可自动注入

```java
@Component
public class Consumer {

    @CantorReference
    HelloService helloService;

    public String sayHello() {
        helloService.sayHello("Cantor RPC Framework!");
    }

}
```

---

### 快速开始(API方式)
API方式在我的博客中有介绍: [QingBlog](http://121.5.56.128/blog/466e124765c2ed548657460d47bbe3c2)

---

> **作者信息**:  
> 我在写这个框架的时候是大四, 也顺便将这个框架作为我的毕业论文, 当时想着编写这个框架, 主要是为了对自己所学知识的综合运用,巩固和强化, 其次也为了能让更多人能通过我这个框架更容易的了解到RPC的底层是如何运作的, 
> 框架中有很多多线程, 动态代理, JUC并发编程和设计模式等代码和思想希望能够给大家提供学习和参考价值.  
> 另外, 本框架也有很多不足之处, 希望得到大家的Pull Request.
> 有兴趣的朋友可以联系作者进群一同探讨.

**QQ群:**  

<img src="https://thq-oss.oss-cn-beijing.aliyuncs.com/images/rpc_group.jpg" width="30%">


---
待优化:

1. 重试策略分三种:   
   异步,任意一个成功即成功(当前使用)  
   异步,全部成功算成功  
   同步,任意一个成功即成功(未来成为默认)
2. 加入roundRobin负载
3. 集成SpringBoot (已完成)
4. Consumer.discoverer监听节点变化
5. 垃圾请求过滤(magicNum)









