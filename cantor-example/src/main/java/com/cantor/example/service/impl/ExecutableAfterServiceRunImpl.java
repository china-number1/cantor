package com.cantor.example.service.impl;

import com.cantor.provider.spi.ExecutableAfterServiceRun;

/**
 * Cantor提供的SPI接口的实现
 * 在Provider端服务被调用后同步执行
 * (具体使用方法参照Java SPI机制: https://www.jianshu.com/p/e4262536000d)
 */
public class ExecutableAfterServiceRunImpl implements ExecutableAfterServiceRun {

    /**
     * @param result 服务方法执行完后产生的结果,你可以修改这个返回值
     * @return 服务最终返回值
     */
    @Override
    public Object execute(Object result) {
        return result;
    }
}
