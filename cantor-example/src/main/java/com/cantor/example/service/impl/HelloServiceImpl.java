package com.cantor.example.service.impl;

import cn.hutool.core.lang.Console;
import com.cantor.example.service.HelloService;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class HelloServiceImpl implements HelloService {

    @Override
    public String sayHello(String word) {
        String result = "你好啊, " + word;
        System.out.println(result);
        return result;
    }

    // 避免Random重复创建,转而采用ThreadLocalRandom
    @Override
    public int fusing(int n, int m) throws Exception {
        Console.log("接收到了{} + {}", n, m);
        // 当n==m时抛异常
        if (n == m) {
            throw new Exception("意料的异常");
        }
        TimeUnit.MILLISECONDS.sleep(500);
        return n + m;
    }

    @Override
    public int cal(int a, int b) {
        return a + b;
    }

    private AtomicInteger count = new AtomicInteger(0);

    @Override
    public boolean loadBalanceCall() {
        Console.error("Be called {} times.", count.incrementAndGet());
        return true;
    }

}
