package com.cantor.example.service.impl;

import com.cantor.provider.spi.ExecutableWhileServiceRunning;

/**
 * Cantor提供的SPI接口的实现
 * 在Provider端服务在执行的同时异步执行
 * (具体使用方法参照Java SPI机制: https://www.jianshu.com/p/e4262536000d)
 */
public class ExecutableServiceWhileRunningImpl implements ExecutableWhileServiceRunning {


    /**
     * 当服务在执行的同时, 异步执行
     */
    @Override
    public void execute() {
        System.err.println("我是业务同时1");
    }
}
