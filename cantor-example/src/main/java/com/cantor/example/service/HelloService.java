package com.cantor.example.service;


public interface HelloService {

    String sayHello(String name);

    int fusing(int n,int m) throws Exception;

    int cal(int a,int b);

    boolean loadBalanceCall();

}
