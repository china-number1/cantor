package com.cantor.example;

import com.cantor.core.center.RegistrationCenter;
import com.cantor.core.center.impl.ZooKeeperRegistrationCenter;
import com.cantor.core.config.CantorAppConfig;
import com.cantor.core.config.CantorAppConfigProperties;
import com.cantor.example.service.HelloService;
import com.cantor.example.service.impl.HelloServiceImpl;
import com.cantor.provider.pojo.ServiceBoat;
import com.cantor.provider.regsitry.ServiceRegistrant;
import com.cantor.provider.start.ProviderBootstrap;

public class TestProvider8869 {
    public static void main(String[] args) throws Exception {

        // 1. 准备CantorAppConfig
        CantorAppConfig app = new CantorAppConfig(new CantorAppConfigProperties()
                .setAppName("provider8869")
                .setPort(8869)
        );

        // 2. 准备远程注册中心
        RegistrationCenter center = new ZooKeeperRegistrationCenter()
                .setAddress("127.0.0.1:2181");

        // 3. 准备服务注册器
        ServiceRegistrant registrant = ServiceRegistrant.DEFAULT_REGISTRANT;

        // 4. 注册
        registrant.register(new ServiceBoat<HelloService>()
                .setServiceInterface(HelloService.class)
                .setServiceImpl(new HelloServiceImpl())
                .setWeight(5)
        );

        // 5. 准备启动器 (传入上面所有组件)
        ProviderBootstrap providerBootstrap = ProviderBootstrap.builder()
                .appConfig(app)
                .center(center)
                .serviceRegistrant(registrant)
                .build();

        // 6. 启动
        providerBootstrap.start();

    }
}
