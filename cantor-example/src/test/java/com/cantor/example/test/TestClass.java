package com.cantor.example.test;

import cn.hutool.core.lang.Console;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ReflectUtil;
import com.cantor.example.service.HelloService;
import com.google.common.base.Defaults;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class TestClass {
//
//    // 测试 空字符串拼接
//    @Test
//    public void testNullAppend() {
//        ServiceBoat boat = new ServiceBoat<HelloService>()
//                .setServiceInterface(HelloService.class)
//                .setServiceImpl(new HelloServiceImpl());
//        System.out.println(boat);
//    }
//
//    // 测试Kryo工具类
//    @Test
//    public void t() {
//        // 测试对一个对象序列化
//        Serializer serializer = new KryoSerializer();
//        byte[] bytes = serializer.serialize("new ComputerVO()");
//        System.out.println(bytes.length);
//        System.out.println(serializer.deserialize(bytes, String.class));
//    }
//
//    // 测试ArrayList
//    @Test
//    public void testArrayList() {
//        KryoSerializer serializer = new KryoSerializer();
//        List<ComputerVO> list = new ArrayList<>();
//        list.add(new ComputerVO());
//        list.add(new ComputerVO());
//        list.add(new ComputerVO());
//        byte[] bytes2 = serializer.serialize(list);
//        System.out.println(bytes2.length);
//        List list2 = serializer.deserialize(bytes2, List.class);
//        list2.forEach(System.out::println);
//    }
//
//    // 测试LinkedList
//    @Test
//    public void testLinkedList() {
//        KryoSerializer serializer = new KryoSerializer();
//        List<ComputerVO> linkedList = new LinkedList<>();
//        linkedList.add(new ComputerVO());
//        linkedList.add(new ComputerVO());
//        byte[] bytes = serializer.serialize(linkedList);
//        System.out.println(bytes.length);
//        List result = serializer.deserialize(bytes, List.class);
//        result.forEach(System.out::println);
//    }
//
//    // 测试HashSet
//    @Test
//    public void testHashSet() {
//        KryoSerializer serializer = new KryoSerializer();
//        Set<ComputerVO> hashSet = new HashSet<>();
//        hashSet.add(new ComputerVO());
//        hashSet.add(new ComputerVO());
//        byte[] bytes = serializer.serialize(hashSet);
//        System.out.println(bytes.length);
//        Set result = serializer.deserialize(bytes, Set.class);
//        result.forEach(System.out::println);
//    }
//
//    // 测试io.protostuff : 普通类
//    @Test
//    public void testIoProtostuff() {
//        ProtobufSerializer serializer = new ProtobufSerializer();
//        byte[] bytes = serializer.serialize(new ComputerVO());
//        System.out.println(bytes.length);
//        System.out.println(serializer.deserialize(bytes, ComputerVO.class));
//    }
//
//    // 测试io.protostuff : ArrayList
//    @Test
//    public void testIoProtostuffArrayList() {
//        Serializer serializer = new ProtobufSerializer();
//        List<ComputerVO> arrayList = new ArrayList<>();
//        arrayList.add(new ComputerVO());
//        arrayList.add(new ComputerVO());
//        arrayList.add(new ComputerVO());
//        byte[] bytes = serializer.serialize(arrayList);
//        System.out.println(bytes.length);
//        serializer.deserialize(bytes, LinkedList.class).forEach(System.out::println);
//    }
//
//    // 测试io.protostuff : LinkedList
//    @Test
//    public void testIoProtostuffLinkedList() {
//        Serializer serializer = new ProtobufSerializer();
//        List<ComputerVO> linkedList = new LinkedList<>();
//        linkedList.add(new ComputerVO());
//        linkedList.add(new ComputerVO());
//        linkedList.add(new ComputerVO());
//        byte[] bytes = serializer.serialize(linkedList);
//        System.out.println(bytes.length);
//        serializer.deserialize(bytes, LinkedList.class).forEach(System.out::println);
//    }
//
//    // 测试io.protostuff : HashMap
//    @Test
//    public void testIoProtostuffHashMap() {
//        Serializer serializer = new ProtobufSerializer();
//        // 复杂嵌套的Map
//        Map<String, HashMap<Long, ComputerVO>> hashMap = new HashMap<>();
//        // el1
//        HashMap<Long, ComputerVO> m1 = new HashMap<>();
//        m1.put(123L, new ComputerVO());
//        hashMap.put("123", m1);
//        // el2
//        HashMap<Long, ComputerVO> m2 = new HashMap<>();
//        m2.put(456L, new ComputerVO().setVar1(3.1415926));
//        hashMap.put("456", m2);
//        byte[] bytes = serializer.serialize(hashMap);
//        System.out.println(bytes.length);
//        HashMap result = serializer.deserialize(bytes, HashMap.class);
//        result.forEach((BiConsumer<String, Map<Long, ComputerVO>>) (k, v) -> {
//            System.out.println("---el---");
//            System.out.println("k:" + k);
//            v.forEach(new BiConsumer<Long, ComputerVO>() {
//                @Override
//                public void accept(Long s, ComputerVO computer) {
//                    System.out.println("string:" + s + ",computer:" + computer);
//                }
//            });
//        });
//    }
//
//    // 测试io.protostuff : LinkedHashMap
//    @Test
//    public void testIoProtostuffLinkedHashMap() {
//        Serializer serializer = new ProtobufSerializer();
//        // 复杂嵌套的Map
//        Map<String, HashMap<Long, ComputerVO>> hashMap = new LinkedHashMap<>();
//        // el1
//        HashMap<Long, ComputerVO> m1 = new HashMap<>();
//        m1.put(123L, new ComputerVO());
//        hashMap.put("123", m1);
//        // el2
//        HashMap<Long, ComputerVO> m2 = new HashMap<>();
//        m2.put(456L, new ComputerVO().setVar1(3.1415926));
//        hashMap.put("456", m2);
//        // ser
//        byte[] bytes = serializer.serialize(hashMap);
//        System.out.println(bytes.length);
//        // deser
//        LinkedHashMap result = serializer.deserialize(bytes, LinkedHashMap.class);
//        result.forEach((BiConsumer<String, Map<Long, ComputerVO>>) (k, v) -> {
//            System.out.println("---el---");
//            System.out.println("k:" + k);
//            v.forEach(new BiConsumer<Long, ComputerVO>() {
//                @Override
//                public void accept(Long s, ComputerVO computer) {
//                    System.out.println("string:" + s + ",computer:" + computer);
//                }
//            });
//        });
//    }
//
//
//    // 测试Jdk序列化 : 普通pojo
//    @Test
//    public void testJdk() {
//        ComputerVO computer = new ComputerVO();
//        Serializer ser = Serializer.allSerializers.get(SerializationTypes.KRYO.getValue());
//        byte[] bytes = ser.serialize(computer);
//        System.out.println(bytes.length);
//        System.out.println(ser.deserialize(bytes, ComputerVO.class));
//    }
//
//    // // 测试Jdk序列化 : String类型
//    @Test
//    public void testJdkString() {
//        String str = "Hello Cantor, 你好Cantor";
//        Serializer ser = Serializer.allSerializers.get(SerializationTypes.JSON.getValue());
//        byte[] bytes = ser.serialize(str);
//        System.out.println(bytes.length);
//        System.out.println(ser.deserialize(bytes, String.class));
//    }
//
//    // 测试Jdk序列化 :  LinkedList
//    @Test
//    public void testJdkLinkedList() {
//        Serializer serializer = Serializer.allSerializers.get(SerializationTypes.JSON.getValue());
//        LinkedList<ComputerVO> linkedList = new LinkedList<>();
//        linkedList.add(new ComputerVO().setVar5("天爱无损"));
//        linkedList.add(new ComputerVO());
//        linkedList.add(new ComputerVO().setVar2(new byte[]{6, 6, 6}));
//        byte[] bytes = serializer.serialize(linkedList);
//        System.out.println(bytes.length);
//        serializer.deserialize(bytes, List.class).forEach(System.out::println);
//    }
//
//    // 测试InetAddress的API
//    @Test
//    public void testInetAddresss() throws IOException {
//        InetAddress localHost = InetAddress.getLocalHost();
//        System.out.println(localHost.getHostAddress()); // 用这个, 这个就是网卡的IP地址
//        System.out.println(localHost.getHostName());
//        System.out.println(localHost.getCanonicalHostName());
//        ServerSocket serverSocket = new ServerSocket();
//    }
//
//    // 测试url拼接
//    @Test
//    public void testUrlJoiner() {
//        UrlBuilder builder = UrlBuilder.create();
//        builder.setScheme("")
//                .setHost("192.168.4.9")
//                .setPort(8868)
//                .addQuery("version", "v2.0")
//                .addQuery("timeout", StrUtil.toString(3000))
//                .addQuery("retries", StrUtil.toString(2))
//                .build();
//        System.out.println(builder.getHost()); // 192.168.4.9
//        System.out.println(builder.getPort()); // 8868
//        System.out.println(builder.getPathStr()); // /
//        System.out.println(builder.getScheme()); //
//        System.out.println(builder.getSchemeWithDefault()); // http
//        System.out.println(builder.getQueryStr()); // version=v2.0&timeout=3000&retries=2
//    }
//
//    // 测试NodePathUtil.buildNodepath
//    @Test
//    public void testNodePathUtil_buildNodePath() {
//        UrlBuilder builder = UrlBuilder.create();
//        final Class<HelloService> service = HelloService.class;
//        builder
//                .setHost("192.168.4.9")
//                .setPort(8868)
//                .addQuery("version", "v2.0")
//                .addQuery("timeout", StrUtil.toString(3000))
//                .addQuery("retries", StrUtil.toString(2))
//                .addQuery("interface", service.getName())
//                .addQuery("methods", CantorUtil.getMethodNamesStr(service));
//        String nodePath = CantorUtil.buildNodePath(builder);
//        System.out.println(nodePath);
//    }
//
//    // 根据节点路径获取对应的数据
//    @Test
//    public void testNodePathGetQuery() {
//        UrlBuilder builder = UrlBuilder.of("http://192.168.1.9:8808?version=&loadBalance=&timeout=0&weight=0&retries=0&mock=&" +
//                "interface=com.cantor.provider.spi.ExecutableWhileServiceRunning.cantor.example.service.HelloService&" +
//                "methods=smile,hi,sayHello");
//        System.out.println(builder.getHost());
//        System.out.println(builder.getPort());
//        System.out.println(builder.getQuery().get("version"));
//        System.out.println(Convert.toInt(builder.getQuery().get("timeout")));
//        System.out.println(Convert.toInt(builder.getQuery().get("retries")));
//        System.out.println(builder.getQuery().get("mock"));
//        System.out.println(builder.getQuery().get("methods"));
//    }
//
//    // 测试DefaultServiceInvoker
//    @Test
//    public void testServiceInvoder() throws Exception {
//        DefaultServiceInvoker invoker = new DefaultServiceInvoker();
//        ServiceBoat boat = new ServiceBoat<HelloService>()
//                .setTimeout(3000);
//        Object result = invoker.invoke(new HelloServiceImpl(), "genericFun", new Class[]{Object.class}, new Object[]{3.14}, boat);
//        System.out.println(result);
//    }
//
//    // 测试获取本机ip
//    @Test
//    @SneakyThrows
//    public void testGetLocalHost() {
//        System.out.println(InetAddress.getLocalHost().getCanonicalHostName());
//        System.out.println(InetAddress.getLocalHost().getHostAddress());
//        System.out.println(InetAddress.getLocalHost().getHostName());
//        System.out.println(InetAddress.getLocalHost().getAddress());
//    }
//
//    // 测试Map<Long>元素比较问题
//    @Test
//    public void testMapLong() {
//        Map<Long, String> map = new ConcurrentHashMap<>();
//        long key = 1;
//        map.put(key, "1");
//
//        Long myKey = new Long(1);
//        System.out.println(map.containsKey(myKey)); // false
//        System.out.println(map.containsKey(Long.valueOf(1))); // true
//    }
//
//    // 测试CompletableFuture.get(time)
//    @Test
//    @SneakyThrows
//    public void testCompletableFutureGet() {
//        CompletableFuture<Object> future = new CompletableFuture<>();
//        new Thread(() -> {
//            try {
//                Thread.sleep(3000);
//                future.thenApplyAsync(new Function<Object, Object>() {
//                    @Override
//                    public Object apply(Object o) {
//                        return "hahahaha";
//                    }
//                });
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        });
//        System.out.println(future.get(5, TimeUnit.SECONDS));
//    }
//
//
//    // 测试序列化
//    @Test
//    public void testSerialization() {
//        CantorMessage msg = CantorRequestMessage.builder()
//                .interfaceName("123")
//                .methodName("456")
//                .parameterTypes(new Class[]{String.class, double.class})
//                .parameterValue(new Object[]{"Hello", 3.14})
//                .returnType(Student.class)
//                .serviceVersion("v2.0")
//                .build();
//        Serializer ser = Serializer.allSerializers.get(SerializationTypes.PROTOBUF.getValue());
//        byte[] bytes = ser.serialize(msg);
//        System.out.println(bytes.length);
//        System.out.println(ser.deserialize(bytes, CantorMessage.class));
//    }
//
//
//    // 测试RandomUtil
//    @Test
//    public void testRandomUtil() {
//        IntStream.range(0, 500).forEach(i -> {
//            int j = RandomUtil.randomInt(1);
//            System.out.println(j);
//        });
//    }
//
//    // 测试CompletableFuture的anyOf
//    @Test
//    @SneakyThrows
//    public void testAnyOf() {
//        CountDownLatch latch = new CountDownLatch(1);
//        CompletableFuture<Object> anyOfFuture = CompletableFuture.anyOf(
//                CompletableFuture.supplyAsync((Supplier<Object>) () -> {
//                    try {
//                        CantorUtil.threadSleep(5000);
//                        latch.countDown();
//                        return "1";
//                    } catch (Exception e) {
//                        return null;
//                    }
//                }),
//                CompletableFuture.supplyAsync(() -> {
//                    try {
//                        CantorUtil.threadSleep(2000);
//                        latch.countDown();
//                        return "2";
//                    } catch (Exception e) {
//                        return null;
//                    }
//                }),
//                CompletableFuture.supplyAsync(() -> {
//                    try {
//                        CantorUtil.threadSleep(3000);
//                        latch.countDown();
//                        return "3";
//                    } catch (Exception e) {
//                        return null;
//                    }
//                })
//        );
//        latch.await();
//        System.out.println("结果: " + anyOfFuture.join());
//    }
//
//    // 测试CompletableFuture的obtrudeValue
//    @Test
//    @SneakyThrows
//    public void testObtrudeValue() {
//        CompletableFuture<Object> future = new CompletableFuture<>();
//        new Thread(() -> {
//            CantorUtil.threadSleep(3000);
//            future.complete("Hello World");
////            future.obtrudeValue("Hello ObtrudeValue"); // 这种方式可以在任何地方强制更改返回值.
//        }).start();
//        future.join();
//        System.out.println(future.get());
//        System.out.println(future.get());
//        future.completeExceptionally(new Exception("测试")); // 已经完成, 这个方法将无效.
//        System.out.println(future.get());
//    }
//
//
//    // 测试几个线程同时为一个变量赋值 (多个线程往同一个变量里赋值,要防止再次操作
//    @Test
//    public void testThreadAssign() {
//        ExecutorService pool = Executors.newCachedThreadPool();
//        CompletableFuture future = new CompletableFuture();
//        pool.execute(() -> {
//            System.out.println(Thread.currentThread().getName()+"执行了");
//            CantorUtil.threadSleep(5000);
//            future.complete(1);
//        });
//        CantorUtil.threadSleep(100);
//        pool.execute(() -> {
//            System.out.println(Thread.currentThread().getName()+"执行了");
//            future.complete(2);
//        });
//        pool.execute(() -> {
//            System.out.println(Thread.currentThread().getName()+"执行了");
//            future.complete(3);
//        });
//
//        Object result = future.join();
//        CantorUtil.threadSleep(10000);
//        future.complete(666);
//        System.err.println("结果: " + result);
//
//    }
//
//
//    // 测试负载多节点选择
//    @Test
//    public void testLbMultiSelection(){
//        List<WeightRandom.WeightObj<String>> candidates = CollUtil.newArrayList();
//        candidates.add(new WeightRandom.WeightObj("A",10));
//        candidates.add(new WeightRandom.WeightObj("B",1));
//        LoadBalance lb = new RandomLoadBalanceStrategy();
//        Collection<String> selectedNode = lb.select(candidates, 30);
//        selectedNode.forEach(System.err::println);
//    }

    // 测试加权轮询负载均衡
    @Test
    public void testRoundRobin(){

    }

    // 测试void方法通过反射获取返回值是什么类型
    @Test
    public void testReturnVoidWithReflect(){
        Method m = ReflectUtil.getMethod(HelloService.class, "loadBalanceCall", new Class[]{});
        System.out.println(m.getReturnType().getName());
    }

    // 测试Hutool保留小数
    @Test
    public void testScale(){
        Console.error("总共{}w次RPC调用结束...", NumberUtil.round(55000 * 1.0f / 10000,1));
    }

    // 测试系统时间为偶数情况
    @Test
    public void testOdd(){
        for (int i = 0; i < 100; i++) {
            Console.log(ThreadLocalRandom.current().nextBoolean());
        }
    }

    // 测试void方法默认值
    @Test
    public void testVoidDefault(){
        class A{
            void fun(){

            }
        }

        Method m = ReflectUtil.getMethod(A.class, "fun", null);
        System.out.println(Defaults.defaultValue(m.getReturnType())); // null
    }

    // char默认值
    @Test
    public void testDefaultChar(){
        char ch = 51;
        System.out.println(ch);
    }

    // 测试加载java.lang.Void
    @Test
    public void testClassForNameVoid() throws Exception {
        Class<?> clz = Class.forName("java.lang.Void");
        Constructor<?> constructor = clz.getDeclaredConstructor(null);
        constructor.setAccessible(true);
        Object voidO = constructor.newInstance();
    }

    // 测试输出几个分布式id
    @Test
    public void testOutIds(){
        for (int i = 0; i < 30; i++) {
            System.out.println(12980+i);
        }
    }

}
