package com.cantor.provider;

import com.cantor.core.center.RegistrationCenter;
import com.cantor.core.center.impl.ZooKeeperRegistrationCenter;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * cantor-provider 测试类
 */
public class TestClass {
    // 测试分布式id
    @Test
    public void testSequenceId() throws IOException {
        RegistrationCenter center = new ZooKeeperRegistrationCenter()
                .setNamespace("test")
                .setSequenceIdZNode("/seq");
        center.run();

        CountDownLatch latch = new CountDownLatch(1);
        for (int i = 0; i < 30; i++) {
            new Thread(()->{
                try {
                    // 30个线程等在这里, 等到countDown()再一起竞争id
                    latch.await();
                    long id = center.getSequenceId();
                    System.err.println(id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }
        latch.countDown();
        System.out.println("End...");
        System.in.read();
    }

    // 测试删除注册中心不存在的节点
    @Test
    public void testAbsentNode() throws InterruptedException {
        RegistrationCenter center = new ZooKeeperRegistrationCenter()
                .setNamespace("test")
                .setSequenceIdZNode("/seq");
        center.run();
        center.deleteNode("test","abc"); // 删除不存在的节点会报异常
        new CountDownLatch(1).await();
    }

}
