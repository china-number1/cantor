package com.cantor.provider.invoker;

import com.cantor.provider.pojo.ServiceBoat;

/**
 * Provider专用服务方法执行器
 * 已弃用, 请移步 ProviderServiceInvoker
 */
@Deprecated
public interface ServiceInvoker {
    Object invoke(Object impl, String methodName,Class[] paramTypes, Object[] paramValues,ServiceBoat boat) throws Exception;
}
