package com.cantor.provider.invoker;

/**
 * Provider端专用的ServiceInvoker接口
 */
public interface ProviderServiceInvoker {
    void invoke();
}
