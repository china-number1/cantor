package com.cantor.provider.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 服务运载器, 将一个ServiceBoat传给注册中心实现类, 既可注册进远程注册中心
 */
@Data
@Accessors(chain = true)
public class ServiceBoat<T> {
    // 版本信息
    private String version = "";

    // 服务接口
    private Class<T> serviceInterface;

    // 服务实现类
    private T serviceImpl;

    // 负载均衡策略(服务端不需要设置负载策略,这是Consumer干的事)
//    private String loadBalance= "";

    // 服务执行多久算超时?
    private int timeout;

    // 权重
    private int weight;

    // 服务降级:强制执行策略,例如force:return null, fail:return null
    private String mock = "";

    // 还有更多参数待更新
    // ...
}
