package com.cantor.provider.regsitry;

import com.cantor.common.exception.NoSuchServiceImplException;
import com.cantor.core.center.RegistrationCenter;
import com.cantor.core.config.CantorAppConfig;
import com.cantor.provider.pojo.ServiceBoat;
import com.cantor.provider.regsitry.impl.DefaultServiceRegistrantImpl;

import java.util.Map;

/**
 * 服务注册器, 维护一个map
 * 注册服务需要传入ServiceBoat(服务运载船)
 * 该类只关注map
 */
public interface ServiceRegistrant {

    ServiceRegistrant DEFAULT_REGISTRANT = new DefaultServiceRegistrantImpl();

    // 注册一个ServiceBoat到Map中
    void register(ServiceBoat boat) throws Exception;

    // 根据名字得到一个ServiceBoat
    <T> ServiceBoat<T> getBoat(String serviceInterfaceName) throws NoSuchServiceImplException;

    // 得到存放服务的map
    Map<String,ServiceBoat> allServiceBoats();

    // 将所有服务元信息注册到注册中心
    void registerAllServicesToCenter(CantorAppConfig appConfig,RegistrationCenter center);

}
