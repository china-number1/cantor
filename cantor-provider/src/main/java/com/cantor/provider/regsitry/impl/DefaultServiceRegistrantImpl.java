package com.cantor.provider.regsitry.impl;

import com.cantor.common.exception.NoSuchServiceImplException;
import com.cantor.provider.pojo.ServiceBoat;
import com.cantor.provider.regsitry.AbstractServiceRegistrant;
import lombok.extern.slf4j.Slf4j;

/**
 * 在拥有正常注册和获取ServiceImpl的基础上. 加一些自己的功能.
 * 例如让返回的ServiceImpl走一遍用户SPI提供的过滤器
 */
@Slf4j
public class DefaultServiceRegistrantImpl extends AbstractServiceRegistrant {

    @Override
    public void register(ServiceBoat boat) {
        super.register(boat);
        log.trace(boat.getServiceInterface().getName()+",此boat被放入map了.");
    }

    @Override
    public <T> ServiceBoat<T> getBoat(String serviceInterfaceName) throws NoSuchServiceImplException {
        log.trace(serviceInterfaceName+"从Map中被get了");
        return super.getBoat(serviceInterfaceName);
    }
}
