package com.cantor.provider.spi;

/**
 * SPI接口(仅限Provider端)
 * 在某一服务执行之前执行
 */
@FunctionalInterface
public interface ExecutableBeforeServiceRun {

    /**
     * 服务方法执行之前执行
     * @param paramValues 服务的方法被调用之前的参数值, 你可以修改这个参数值数组
     */
    void execute(Object[] paramValues);
}
