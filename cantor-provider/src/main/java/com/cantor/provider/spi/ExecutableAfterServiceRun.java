package com.cantor.provider.spi;

/**
 * SPI接口(仅限Provider端)
 * 当服务某一方法执行完毕后, 执行该接口
 */
@FunctionalInterface
public interface ExecutableAfterServiceRun {

    /**
     * @param result 服务方法执行完后产生的结果,你可以修改这个返回值
     * @return 服务最终返回值
     */
    Object execute(Object result);
}
