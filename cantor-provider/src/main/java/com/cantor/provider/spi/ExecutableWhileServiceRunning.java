package com.cantor.provider.spi;

/**
 * SPI接口(仅限Provider端)
 * 当某一服务方法正在执行的时, 与其异步同时运行.
 */
@FunctionalInterface
public interface ExecutableWhileServiceRunning {

    /**
     * 当服务在执行的同时, 异步执行
     */
    void execute();
}
